﻿#include <iostream>
#include <string>

int main()
{
    std::cout << "Enter text: ";
    std::string Text;
    std::getline(std::cin, Text);

    std::cout << "\nNumber of symbols: " << Text.length() << "\n";
    std::cout << "First symbol - " << Text[0] << "\n";
    std::cout << "Last symbol - " << Text[Text.length() - 1] << "\n";
    return 0;
    }
